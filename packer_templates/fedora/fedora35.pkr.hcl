variables {
    iso_url = "http://download.fedoraproject.org/pub/fedora/linux/releases/35/Server/x86_64/iso/Fedora-Server-dvd-x86_64-35-1.2.iso"
    iso_checksum = "md5:1b77c49bb1d8bc2fe874ab380e5615de"
    # Todo: избавиться от inst.updates. См. https://bugzilla.redhat.com/show_bug.cgi?id=2019579
    boot_command = [
        "<up><wait><tab> inst.text inst.updates=http://rvykydal.fedorapeople.org/update-images/updates.f35-2019579-resolvconf.img inst.ks=http://{{ .HTTPIP }}:{{ .HTTPPort }}/ks.cfg<enter>"
    ]
}

source "qemu" "fedora35" {
    iso_url = var.iso_url
    iso_checksum = var.iso_checksum
    shutdown_command = "echo 'password' | sudo -S shutdown -P now"
    disk_size = "30000M"
    memory = 5120
    format = "qcow2"
    accelerator = "kvm"
    http_content = {
        "/ks.cfg" = templatefile("${path.root}/fedora35.pkrtpl", {groups = []})
    }
    ssh_username = "vagrant"
    ssh_password = "password"
    ssh_timeout = "50m"
    vm_name = "${source.name}"
    net_device = "virtio-net"
    disk_interface = "virtio"
    boot_wait = "5s"
    boot_command = var.boot_command
}

source "qemu" "fedora35-kde" {
    iso_url = var.iso_url
    iso_checksum = var.iso_checksum
    shutdown_command = "echo 'password' | sudo -S shutdown -P now"
    disk_size = "30000M"
    memory = 5120
    format = "qcow2"
    accelerator = "kvm"
    http_content = {
        "/ks.cfg" = templatefile("${path.root}/fedora35.pkrtpl", {groups = ["kde-desktop"]})
    }
    ssh_username = "vagrant"
    ssh_password = "password"
    ssh_timeout = "50m"
    vm_name = "${source.name}"
    net_device = "virtio-net"
    disk_interface = "virtio"
    boot_wait = "5s"
    boot_command = var.boot_command
}

build {
    sources = ["source.qemu.fedora35", "source.qemu.fedora35-kde"]
    provisioner "shell" {
        expect_disconnect = true
        scripts = [
            "${path.root}/scripts/crypto-policy.sh",
            "${path.root}/scripts/update.sh",
            "${path.root}/../common/reboot.sh",
            "${path.root}/scripts/cleanup.sh",
            "${path.root}/../common/x.sh",
            "${path.root}/../common/vagrant.sh",
            "${path.root}/../common/love.sh",
            "${path.root}/../common/machine-id-and-random-seed.sh",
            "${path.root}/../common/logs-and-cache.sh",
            "${path.root}/../common/minimize.sh"
        ]
    }
    post-processor "vagrant" {
        output = "${source.name}.box"
        vagrantfile_template = "files/Vagrantfile"
    }
}
