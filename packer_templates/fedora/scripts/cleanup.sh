#!/bin/bash

set -e

sudo rm -f /root/anaconda-ks.cfg /root/original-ks.cfg

rpm -qa | grep 'kernel-[1-9].*' | grep -v `uname -r` | xargs sudo dnf remove -y
rpm -qa | grep 'kernel-core-[1-9].*' | grep -v `uname -r` | xargs sudo dnf remove -y

sudo dnf -y remove gc smartmontools lvm2
sudo dnf -y remove kf5-akonadi-server-mysql community-mysql community-mysql-common community-mysql-server mariadb mariadb-common mariadb-server qt5-qtbase-mysql
sudo dnf -y remove linux-firmware
sudo dnf -y autoremove
# Clean all package cache information.
sudo dnf -y clean all --enablerepo=\*
