variables {
    iso_url = "https://files.red-soft.ru/redos/7.3/x86_64/iso/redos-MUROM-7.3.2-20221027.0-Everything-x86_64-DVD1.iso"
    iso_checksum = "md5:9190eb473db06477ccf4691c52b6871a"
}

source "qemu" "murom" {
    iso_url = var.iso_url 
    iso_checksum = var.iso_checksum 
    shutdown_command = "echo 'password' | sudo -S shutdown -P now"
    disk_size = "30000M"
    memory = 5120
    format = "qcow2"
    accelerator = "kvm"
    http_content = {
        "/ks.cfg" = templatefile("${path.root}/murom.pkrtpl", {groups = ["base", "core"]})
    }
    ssh_username = "vagrant"
    ssh_password = "password"
    ssh_timeout = "50m"
    vm_name = "${source.name}"
    net_device = "virtio-net"
    disk_interface = "virtio"
    boot_wait = "5s"
    boot_command = [
        "<up><wait><tab> inst.text inst.ks=http://{{ .HTTPIP }}:{{ .HTTPPort }}/ks.cfg<enter>"
    ]
}

source "qemu" "murom-mate" {
    iso_url = var.iso_url 
    iso_checksum = var.iso_checksum 
    shutdown_command = "echo 'password' | sudo -S shutdown -P now"
    disk_size = "30000M"
    memory = 5120
    format = "qcow2"
    accelerator = "kvm"
    http_content = {
        "/ks.cfg" = templatefile("${path.root}/murom.pkrtpl", {groups = ["base", "core", "fonts", "mate-desktop", "x11"]})
    }
    ssh_username = "vagrant"
    ssh_password = "password"
    ssh_timeout = "50m"
    vm_name = "${source.name}"
    net_device = "virtio-net"
    disk_interface = "virtio"
    boot_wait = "5s"
    boot_command = [
        "<up><wait><tab> inst.text inst.ks=http://{{ .HTTPIP }}:{{ .HTTPPort }}/ks.cfg<enter>"
    ]
}

build {
    sources = ["source.qemu.murom", "source.qemu.murom-mate"]
    provisioner "shell" {
        scripts = [
            "${path.root}/../common/x.sh",
            "${path.root}/../common/vagrant.sh",
            "${path.root}/../common/love.sh",
            "${path.root}/../common/machine-id-and-random-seed.sh",
            "${path.root}/../common/logs-and-cache.sh",
            "${path.root}/../common/minimize.sh"
        ]
    }
    post-processor "vagrant" {
        output = "${source.name}.box"
        vagrantfile_template = "files/Vagrantfile"
    }
}
