#!/bin/bash

set -e

sudo apt-get -y purge installation-report
sudo apt-get -y purge chromium firefox-esr
sudo apt-get -y purge mysql-common mariadb-common
sudo apt-get -y purge libreoffice-common java-common vlc-bin vlc-data drakon-editor p7zip-full gimp k3b kamera
sudo apt-get -y autoremove

# Clean an apt cache.
sudo apt-get -y clean
