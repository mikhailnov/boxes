#!/bin/bash

set -e

sudo apt-get update
sudo apt-get -y install gosjava-jre

if apt show gosjava-jdk
then
    sudo apt-get -y install gosjava-jdk
fi
