.PHONY: all clean \
	astralinux smolensk-1.6 smolensk-1.7 smolensk-fly-1.7 orel-1.7 \
	basealt aronia-starterkit aronia-server aronia-workstation aronia-simply \
	debian bullseye bullseye-kde \
	fedora fedora35 fedora35-kde fedora36 fedora36-kde \
	lab50 gosjava8 gosjava11 \
	nppkt onyx onyx-kde \
	redsoft murom murom-mate \
	rosa chrome-kde fresh cobalt

all: astralinux basealt debian fedora lab50 nppkt redsoft rosa

clean:
	rm -f packer_templates/astralinux/*.box
	rm -f packer_templates/basealt/*.box
	rm -f packer_templates/debian/*.box
	rm -f packer_templates/fedora/*.box
	rm -f packer_templates/nppkt/*.box
	rm -f packer_templates/redsoft/*.box
	rm -f packer_templates/rosa/*.box

astralinux: smolensk-1.6 smolensk-1.7 smolensk-fly-1.7 orel-1.7

smolensk-1.6:
	rm -f packer_templates/astralinux/smolensk-1.6.box
	cd packer_templates/astralinux; packer build -only qemu.smolensk astra-1.6.pkr.hcl

smolensk-1.7:
	rm -f packer_templates/astralinux/smolensk-1.7.box
	cd packer_templates/astralinux; packer build -only qemu.smolensk astra-1.7.pkr.hcl

smolensk-fly-1.7:
	rm -f packer_templates/astralinux/smolensk-fly-1.7.box
	cd packer_templates/astralinux; packer build -only qemu.smolensk-fly astra-1.7.pkr.hcl

orel-1.7:
	rm -f packer_templates/astralinux/orel-1.7.box
	cd packer_templates/astralinux; packer build -only qemu.orel astra-1.7.pkr.hcl

basealt: aronia-starterkit aronia-server aronia-workstation aronia-simply

aronia-starterkit:
	rm -f packer_templates/basealt/aronia-starterkit.box
	cd packer_templates/basealt; packer build -only qemu.starterkit aronia.pkr.hcl

aronia-server:
	rm -f packer_templates/basealt/aronia-server.box
	cd packer_templates/basealt; packer build -only qemu.server aronia.pkr.hcl

aronia-workstation:
	rm -f packer_templates/basealt/aronia-workstation.box
	cd packer_templates/basealt; packer build -only qemu.workstation aronia.pkr.hcl

aronia-simply:
	rm -f packer_templates/basealt/aronia-simply.box
	cd packer_templates/basealt; packer build -only qemu.simply aronia.pkr.hcl

debian: bullseye bullseye-kde

bullseye:
	rm -f packer_templates/debian/bullseye.box
	cd packer_templates/debian; packer build -only qemu.bullseye bullseye.pkr.hcl

bullseye-kde:
	rm -f packer_templates/debian/bullseye-kde.box
	cd packer_templates/debian; packer build -only qemu.bullseye-kde bullseye.pkr.hcl

fedora: fedora35 fedora35-kde fedora36 fedora36-kde

fedora35:
	rm -f packer_templates/fedora/fedora35.box
	cd packer_templates/fedora; packer build -only qemu.fedora35 fedora35.pkr.hcl

fedora35-kde:
	rm -f packer_templates/fedora/fedora35-kde.box
	cd packer_templates/fedora; packer build -only qemu.fedora35-kde fedora35.pkr.hcl

fedora36:
	rm -f packer_templates/fedora/fedora36.box
	cd packer_templates/fedora; packer build -only qemu.fedora36 fedora36.pkr.hcl

fedora36-kde:
	rm -f packer_templates/fedora/fedora36-kde.box
	cd packer_templates/fedora; packer build -only qemu.fedora36-kde fedora36.pkr.hcl

lab50: gosjava8 gosjava11

gosjava8:
	rm -f packer_templates/lab50/gosjava8.box
	cd packer_templates/lab50; packer build -only qemu.gosjava8 gosjava.pkr.hcl

gosjava11:
	rm -f packer_templates/lab50/gosjava11.box
	cd packer_templates/lab50; packer build -only qemu.gosjava11 gosjava.pkr.hcl

nppkt: onyx onyx-kde

onyx:
	rm -f packer_templates/nppkt/onyx.box
	cd packer_templates/nppkt; packer build -only qemu.onyx onyx.pkr.hcl

onyx-kde:
	rm -f packer_templates/nppkt/onyx-kde.box
	cd packer_templates/nppkt; packer build -only qemu.onyx-kde onyx.pkr.hcl

redsoft: murom murom-mate

murom:
	rm -f packer_templates/redsoft/murom.box
	cd packer_templates/redsoft; packer build -only qemu.murom murom.pkr.hcl

murom-mate:
	rm -f packer_templates/redsoft/murom-mate.box
	cd packer_templates/redsoft; packer build -only qemu.murom-mate murom.pkr.hcl

rosa: chrome-kde fresh cobalt

chrome-kde:
	rm -f packer_templates/rosa/chrome-kde.box
	cd packer_templates/rosa; packer build -only qemu.chrome-kde rosa.pkr.hcl

fresh:
	rm -f packer_templates/rosa/fresh.box
	cd packer_templates/rosa; packer build -only qemu.fresh rosa.pkr.hcl

cobalt:
	rm -f packer_templates/rosa/cobalt.box
	cd packer_templates/rosa; packer build -only qemu.cobalt rosa.pkr.hcl
